import 'package:flutter/material.dart';
import 'package:poc_ngbs/localization/language/languages.dart';
import 'package:poc_ngbs/utilities/colors.dart';

class Tabstransferfavorities extends StatefulWidget {
  final int selectedTab;
  final Function(String value) onTapTabs;
  Tabstransferfavorities(
      {Key key, @required this.selectedTab, @required this.onTapTabs})
      : super(key: key);

  @override
  State<Tabstransferfavorities> createState() => _TabstransferfavoritiesState();
}

class _TabstransferfavoritiesState extends State<Tabstransferfavorities> {
  @override
  Widget build(BuildContext context) {
    List tabsList = [
      Languages.of(context).transferNewRecentTab,
      Languages.of(context).transferNewFavoritesTab,
      Languages.of(context).transferNewContactsTab,
      Languages.of(context).transferNewMyaccountTab,
    ];

    Widget itemTabs({
      String title,
    }) {
      if (tabsList[widget.selectedTab] == title) {
        return Container(
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: buttonColor,
            // boxShadow: [
            //   BoxShadow(
            //     color: buttonColor.withOpacity(0.2),
            //     spreadRadius: 5,
            //     blurRadius: 7,
            //     offset: Offset(0, 3), // changes position of shadow
            //   ),
            // ],
          ),
          child: Center(
            child: Text(
              title,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
            ),
          ),
        );
      }

      return InkWell(
        onTap: () {
          int index = tabsList.indexOf(title);

          widget.onTapTabs(index.toString());
        },
        child: Container(
          padding: EdgeInsets.all(8),
          child: Center(
              child: Text(
            title,
            style: TextStyle(
              fontSize: 14,
              // color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          )),
        ),
      );
    }

    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(left: 10, right: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Color.fromARGB(255, 245, 245, 245),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: tabsList.map((item) => itemTabs(title: item)).toList(),
      ),
    );
  }
}
